package com.example.myproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Encrypt_Activity extends AppCompatActivity {

    TextView submit,encrpt_string;
    EditText string_no;
    String text_string;
    StringBuilder stringBuilder;
    String before_textChange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.encrypt_layout);

        submit = findViewById(R.id.submit_btn);
        string_no = findViewById(R.id.string_no);
        encrpt_string = findViewById(R.id.encrpt_string);



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (string_no.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Please Enter the text to Encrypt",Toast.LENGTH_LONG).show();
                }else {

                    text_string = string_no.getText().toString();
                    String encryptString=  encrypt(text_string);
                    encrpt_string.setText(encryptString);
                }



                }


        });



    }
    public String encrypt(String encryptedString){
        stringBuilder= new StringBuilder();

        for (int i = 0; i < encryptedString.length() - 1; i++)
        {

            // Counting occurrences of s[i]
            int count = 1;
            while (encryptedString.charAt(i) == encryptedString.charAt(i + 1))
            {
                i++;
                count++;
                if(i + 1 == encryptedString.length())
                    break;
            }
            String charCount= String.valueOf(count);
            stringBuilder.append(encryptedString.charAt(i)+charCount);
            System.out.println(encryptedString.charAt(i) + "" +
                    count + " ");
        }
        return String.valueOf(stringBuilder);
    }

}

package com.example.myproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Decrypt_Activity extends AppCompatActivity {
    TextView submit,encrpt_string;
    EditText string_no;
    String text_string;
    StringBuilder stringBuilder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.encrypt_layout);
        submit = findViewById(R.id.submit_btn);
        string_no = findViewById(R.id.string_no);
        encrpt_string = findViewById(R.id.encrpt_string);



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (string_no.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Please Enter the text to Decrypt",Toast.LENGTH_LONG).show();
                }else {
                    text_string = string_no.getText().toString();
                    String decryptString=  decrypt(text_string);
                    encrpt_string.setText(decryptString);
                }




            }


        });
    }

    public String decrypt(String decrypt_string){
        String decrypt_string_space=decrypt_string.replace(" "," 0");
        char[] decrypt_string_array = decrypt_string_space.toCharArray();
        int num=decrypt_string_array.length/2;
        int count=0;
        stringBuilder= new StringBuilder();

        for(int i=count;i<num;i++){
            int char_count=decrypt_string_array[count+1];
            if (Character.getNumericValue(char_count)==0){
                stringBuilder.append(" ");
            }
            for (int j=0;j<Character.getNumericValue(char_count);j++){
                stringBuilder.append(decrypt_string_array[count]);
            }
            count++;
            count++;
        }

        return String.valueOf(stringBuilder);
    }
}
